#!/bin/bash

# go to Montage/script
cd "$( dirname "${BASH_SOURCE[0]}" )"
# go to Montage
cd ..

backup_dir="/home/${USER}/Montage/backup"
backup_prefix="${backup_dir}/snap_"
outfile_dir=data/snap/
TASK_LENGTH=19 # length of each workload in second
# THREADS=(1 12 20 36 40 62 80 90)
THREADS=(40)
SP_THREADS=40
CP_INTERVALS=(5)
# CHUNK_SIZE=(64 128 256 512 1024) # in MB
CHUNK_SIZE=(64) # in MB

mkdir -p ${backup_dir}
mkdir -p ${outfile_dir}
REPEAT_NUM=1 # number of trials

queue_test="QueueChurn:eq50dq50:prefill=2000"

map_test_write="MapChurnTest<string>:g0p0i50rm50:range=1000000:prefill=500000"
map_test_readmost="MapChurnTest<string>:g90p0i5rm5:range=1000000:prefill=500000"
map_test_5050="MapChurnTest<string>:g50p0i25rm25:range=1000000:prefill=500000"

delete_heap_file(){
    rm -rf /mnt/pmem/${USER}* /mnt/pmem/savitar.cat /mnt/pmem/psegments
    rm -f /mnt/pmem/*.log /mnt/pmem/snapshot*
    rm -f ${backup_dir}/*
}

queue_init(){
    echo "Running queue, 50enq 50deq for $TASK_LENGTH seconds"

    rm -rf $outfile_dir/queue.csv
    echo "test,snapshottingType,useDCL,chunkSize,snapTime,chunkChanged,thread,ops,testClass,workloadClass" > $outfile_dir/queue.csv
}

# 1. queues
queue_execute(){
    echo "snapshottingType: ${snapshottingType}, useDCL: ${useDCL}"
    make -j

    for ((i=1; i<=REPEAT_NUM; ++i))
    do
        for threads in "${THREADS[@]}"
        do
            for cp_interval in "${CP_INTERVALS[@]}"
            do
                for chunkSize in "${CHUNK_SIZE[@]}"
                do
                    delete_heap_file
                    echo -n "queue,${snapshottingType},${useDCL}," >> $outfile_dir/queue.csv
                    ./bin/main -R MontageQueue -M$queue_test -t $threads -i $TASK_LENGTH -dtakeSnapshot -dsnapshottingType=${snapshottingType} -duseDCL=${useDCL} -dsnapInterval=${cp_interval} -dbackupFile=${backup_prefix} -dchunkSize=${chunkSize} -dspThreads=${SP_THREADS} | tee -a $outfile_dir/queue.csv
                    delete_heap_file
                done
            done
        done
    done
}

map_init(){
    echo "Running maps, g0i50r50, g50i25r25 and g90i5r5 for $TASK_LENGTH seconds"

    rm -rf $outfile_dir/g0i50r50.csv $outfile_dir/g50i25r25.csv $outfile_dir/g90i5r5.csv
    echo "test,snapshottingType,useDCL,chunkSize,snapTime,chunkChanged,thread,ops,testClass,workloadClass" > $outfile_dir/g0i50r50.csv
    echo "test,snapshottingType,useDCL,chunkSize,snapTime,chunkChanged,thread,ops,testClass,workloadClass" > $outfile_dir/g50i25r25.csv
    echo "test,snapshottingType,useDCL,chunkSize,snapTime,chunkChanged,thread,ops,testClass,workloadClass" > $outfile_dir/g90i5r5.csv
}

map_execute(){
    echo "snapshottingType: ${snapshottingType}, useDCL: ${useDCL}"
    make -j

    for ((i=1; i<=REPEAT_NUM; ++i))
    do
        for threads in "${THREADS[@]}"
        do
            for cp_interval in "${CP_INTERVALS[@]}"
            do
                for chunkSize in "${CHUNK_SIZE[@]}"
                do
                    delete_heap_file
                    echo -n "g0i50r50,${snapshottingType},${useDCL}," >> $outfile_dir/g0i50r50.csv
                    ./bin/main -R MontageHashTable -M $map_test_write -t $threads -i $TASK_LENGTH -dtakeSnapshot -dsnapshottingType=${snapshottingType} -duseDCL=${useDCL} -dsnapInterval=${cp_interval} -dbackupFile=${backup_prefix} -dchunkSize=${chunkSize} -dspThreads=${SP_THREADS} | tee -a $outfile_dir/g0i50r50.csv

                    delete_heap_file
                    echo -n "g50i25r25,${snapshottingType},${useDCL}," >> $outfile_dir/g50i25r25.csv
                    ./bin/main -R MontageHashTable -M $map_test_5050 -t $threads -i $TASK_LENGTH -dtakeSnapshot -dsnapshottingType=${snapshottingType} -duseDCL=${useDCL} -dsnapInterval=${cp_interval} -dbackupFile=${backup_prefix} -dchunkSize=${chunkSize} -dspThreads=${SP_THREADS} | tee -a $outfile_dir/g50i25r25.csv

                    delete_heap_file
                    echo -n "g90i5r5,${snapshottingType},${useDCL}," >> $outfile_dir/g90i5r5.csv
                    ./bin/main -R MontageHashTable -M $map_test_readmost -t $threads -i $TASK_LENGTH -dtakeSnapshot -dsnapshottingType=${snapshottingType} -duseDCL=${useDCL} -dsnapInterval=${cp_interval} -dbackupFile=${backup_prefix} -dchunkSize=${chunkSize} -dspThreads=${SP_THREADS} | tee -a $outfile_dir/g90i5r5.csv

                    delete_heap_file
                done
            done
        done
    done
}

########################
###       Main       ###
########################
SNAP_TYPES=("online" "stop-the-world")
USE_DCLS=("true" "false")

queue_init
map_init

for snapshottingType in "${SNAP_TYPES[@]}"
do
    for useDCL in "${USE_DCLS[@]}"
    do
        queue_execute
        map_execute
    done
done

