#!/bin/bash

backupDir="/home/mmoridi/Montage/backup"
backup="${backupDir}/snap_mmoridi_mon_000000_sb"
origCopy="${backupDir}/mmoridi_mon_000000_sb"
orig="/mnt/pmem/mmoridi_mon_000000_sb"
concat="${backupDir}/concat"

echo "Show diff for descriptor"
diff /mnt/pmem/mmoridi_mon_000000_desc ${backupDir}/snap_mmoridi_mon_000000_desc_1_0

echo "Show diff for superblocks"

for ((i = 0; i < 9; i++))
do
    if [ -e "${backup}_1_${i}" ]; then
        cat "${backup}_1_${i}" >> ${concat}
        echo "${backup}_1_${i} exists!"

    else
        cat "${backup}_0_${i}" >> ${concat}
    fi
done

diff ${orig} ${concat}

echo "Started copying!"
cp ${orig} ${origCopy}

cmp --verbose ${origCopy} ${concat} > diff.log
