# Montage with Snapshotting

Please see the [original Montage README.md file](https://github.com/urcs-sync/Montage/blob/master/README.md) for instructions on how to build and run Montage.
The instructions below explain how to enable snapshotting.

## Snapshotting Script

`script/run_snapshot.sh` automates testing Montage's memory-mapped file snapshotting with various configurations.

### Configurable Parameters

- **THREADS**: Number of threads to run the workload. Modify as needed (e.g., `THREADS=(40)`).
- **CP_INTERVALS**: Snapshot interval in seconds (e.g., `CP_INTERVALS=(5)`).
- **CHUNK_SIZE**: Size of each chunk in MB (e.g., `CHUNK_SIZE=(64)`).
- **SNAP_TYPES**: Snapshotting strategy: `online`, `stop-the-world`.
- **USE_DCLS**: Use Double-Checked Locking (DCL): `true`, `false`.
- **REPEAT_NUM**: Number of trials (e.g., `REPEAT_NUM=1`).

### Workloads

1. **Queue Test**: 50% enqueue, 50% dequeue.
2. **Map Tests**:
   - `g0i50r50`: 50% insertions, 50% removals.
   - `g50i25r25`: 50% gets, 25% insertions, 25% removals.
   - `g90i5r5`: 90% gets, 5% insertions, 5% removals.

### Output

Results are saved as CSV files in `data/snap/`:
- `queue.csv`, `g0i50r50.csv`, `g50i25r25.csv`, `g90i5r5.csv`.

Each file contains:
- `test`, `snapshottingType`, `useDCL`, `chunkSize`, `snapTime`, `chunkChanged`, `thread`, `ops`.

### Usage

Run the script:
```bash
cd script
./run_snapshot.sh
```

# Experiments

<TODO>
