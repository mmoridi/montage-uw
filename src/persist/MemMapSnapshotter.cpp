#include "MemMapSnapshotter.hpp"
#include "EpochSys.hpp"
#include "EpochAdvancers.hpp"

void MemMapSnapshotter::snapshotThread(int task_num) {
    pds::EpochSys::tid = task_num + 1; // set tid to be one after epoch advancer
    
    // SnapInterval's unit is second.
    int64_t snapInterval = stoi(gtc->getEnv("snapInterval"));

    while(!started.load()) {}
    while(started.load()){
        this_thread::sleep_for(chrono::seconds(snapInterval));
      
        auto snapStart = chrono::high_resolution_clock::now();
        takeSnapshot();
        snapLength += chrono::duration_cast<chrono::milliseconds>(
                chrono::high_resolution_clock::now() - snapStart).count();
    }

    if ((global_sn - 1)) {
        snapLength /= (global_sn - 1);
        printf("%ld,%ld,", snapLength, chunkChangedCount / (global_sn - 1));
    }
}

MemMapSnapshotter* MemMapSnapshotter::getInstance(GlobalTestConfig* _gtc, std::string _heap_file_id, char* _heap_base_adr[]) {
    if (_gtc->checkEnv("snapshottingType") && (_gtc->getEnv("snapshottingType") == "online")) {
        if (_gtc->checkEnv("useDCL") && (_gtc->getEnv("useDCL") == "1")) {
            return new OnlineSnapshotterWithDCL(_gtc, _heap_file_id, _heap_base_adr);
        } else {
            return new OnlineSnapshotterWithoutDCL(_gtc, _heap_file_id, _heap_base_adr);
        }
    } else {
        if (_gtc->checkEnv("useDCL") && (_gtc->getEnv("useDCL") == "1")) {
            return new StopTheWorldSnapshotterWithDCL(_gtc, _heap_file_id, _heap_base_adr);
        } else {
            return new StopTheWorldSnapshotterWithoutDCL(_gtc, _heap_file_id, _heap_base_adr);
        }
    }
}

/**
 * OnlineSnapshotterWithoutDCL
 */
OnlineSnapshotterWithoutDCL::OnlineSnapshotterWithoutDCL(
        GlobalTestConfig* _gtc, std::string _heap_file_id, char* _heap_base_adr[]):
MemMapSnapshotter(_gtc, _heap_file_id, _heap_base_adr) {
    memMapLock = new MemMapLock(numChunk);
    
    /* 
     * takeSnapshot is invoked in the constructor 
     * to copy the pre-allocated heap files to improve the 
     * first snapshotting performance.
     *
     * This function call might take a while as 
     * it is copying the whole memory-mapped file.
    */
    initBackupMemMaps();
    takeSnapshot();

    snapLength = 0;
    chunkChangedCount = 0;
    collisions = 0;

    started.store(false);

    snapshotter_thread = std::move(std::thread(&MemMapSnapshotter::snapshotThread, this, gtc->task_num));
    started.store(true);
}

void OnlineSnapshotterWithoutDCL::takeSnapshot() {
    for (uint64_t chunkId = 0; chunkId < numChunk; chunkId++) {
        memMapLock->lockExclusive(chunkId);
        
        if (chunkChanged[chunkId].ui.load()) {
            initMemMap(chunkId);
            copyChunked(chunkId);
            chunkChangedCount++;
        }
        chunkCopied[chunkId].ui.store(true);
        memMapLock->unlockExclusive(chunkId);
    }

    for (uint64_t i = 0; i < numChunk; i++) {
        memMapLock->lockExclusive(i);
    }

    copyAll(DESC_IDX);
    copyAll(META_IDX);

    closeMemMaps();

    global_sn++;

    for (uint64_t i = 0; i < numChunk; i++) {
        memMapLock->unlockExclusive(i);
    }
}

void OnlineSnapshotterWithoutDCL::clwbProtected(void* addr) {
    clwbLocked(addr);
}

void OnlineSnapshotterWithoutDCL::clwbRangeNofenceProtected(void* addr, uint64_t size) {
    clwbRangeNofenceLocked(addr, size);
}

void OnlineSnapshotterWithoutDCL::clwbLocked(void* addr) {
    uint64_t chunkId = getChunkIdx((uint64_t)addr);
    memMapLock->lockShared(chunkId);
    
    persist_func::clwb(addr);
    markChunkAndChangeBackup(addr, CACHE_LINE_SIZE);
    
    memMapLock->unlockShared(chunkId);
}

void OnlineSnapshotterWithoutDCL::clwbRangeNofenceLocked(void* addr, uint64_t size) {
    uint64_t chunkId = getChunkIdx((uint64_t)addr);
    memMapLock->lockShared(chunkId);
    
    persist_func::clwb_range_nofence(addr, size);
    markChunkAndChangeBackup(addr, size);
    
    memMapLock->unlockShared(chunkId);
}

/**
 * OnlineSnapshotterWithDCL
 */
OnlineSnapshotterWithDCL::OnlineSnapshotterWithDCL(GlobalTestConfig* _gtc,
        std::string _heap_file_id, char* _heap_base_adr[]):
isSnapshotterWorking(false),
OnlineSnapshotterWithoutDCL(_gtc, _heap_file_id, _heap_base_adr)
{ 
}

void OnlineSnapshotterWithDCL::clwbProtected(void* addr) {
    bool copyToBackup = false;

    if (!isSnapshotterWorking.ui.load()) {

        persist_func::clwb(addr);
        markChunk(addr, CACHE_LINE_SIZE);

        if (isSnapshotterWorking.ui.load())
            copyToBackup = true;
    } else {
        clwbLocked(addr);
    }

    if (copyToBackup) clwbLocked(addr);
}

void OnlineSnapshotterWithDCL::clwbRangeNofenceProtected(
        void* addr, uint64_t size) {
    bool copyToBackup = false;

    if (!isSnapshotterWorking.ui.load()) {
        persist_func::clwb_range_nofence(addr, size);
        markChunk(addr, size);

        if (isSnapshotterWorking.ui.load())
            copyToBackup = true;
    } else {
        clwbRangeNofenceLocked(addr, size);
    }

    if (copyToBackup) clwbRangeNofenceLocked(addr, size);
}

void OnlineSnapshotterWithDCL::takeSnapshot() {
    isSnapshotterWorking.ui.store(true);
    OnlineSnapshotterWithoutDCL::takeSnapshot();
    isSnapshotterWorking.ui.store(false);
}

/**
 * StopTheWorldSnapshotterWithoutDCL
 */
StopTheWorldSnapshotterWithoutDCL::StopTheWorldSnapshotterWithoutDCL(GlobalTestConfig* _gtc,
        std::string _heap_file_id, char* _heap_base_adr[]):
MemMapSnapshotter(_gtc, _heap_file_id, _heap_base_adr) {
    // Stop-the-World snapshotter only needs one lock
    memMapLock = new MemMapLock(StopTheWorldSnapshotterWithoutDCL::NUM_LOCK_FOR_STOP_THE_WORLD);

    /* 
     * takeSnapshot is invoked in the constructor 
     * to copy the pre-allocated heap files to improve the 
     * first snapshotting performance.
     *
     * This function call might take a while as 
     * it is copying the whole memory-mapped file.
    */
    initBackupMemMaps();
    takeSnapshot();

    snapLength = 0;
    chunkChangedCount = 0;
    collisions = 0;

    started.store(false);

    snapshotter_thread = std::move(std::thread(&MemMapSnapshotter::snapshotThread, this, gtc->task_num));
    started.store(true);
}

void StopTheWorldSnapshotterWithoutDCL::takeSnapshot() {
    memMapLock->lockExclusive(StopTheWorldSnapshotterWithoutDCL::NUM_LOCK_FOR_STOP_THE_WORLD - 1);
    
    for (uint64_t chunkId = 0; chunkId < numChunk; chunkId++) {
        
        if (chunkChanged[chunkId].ui.load()) {
            initMemMap(chunkId);
            copyChunked(chunkId);
            chunkChangedCount++;
        }
        chunkCopied[chunkId].ui.store(true);
    }

    copyAll(DESC_IDX);
    copyAll(META_IDX);

    closeMemMaps();

    global_sn++;

    memMapLock->unlockExclusive(StopTheWorldSnapshotterWithoutDCL::NUM_LOCK_FOR_STOP_THE_WORLD - 1);
}

void StopTheWorldSnapshotterWithoutDCL::clwbProtected(void* addr) {
    clwbLocked(addr);
}

void StopTheWorldSnapshotterWithoutDCL::clwbRangeNofenceProtected(void* addr, uint64_t size) {
    clwbRangeNofenceLocked(addr, size);
}

void StopTheWorldSnapshotterWithoutDCL::clwbLocked(void* addr) {
    memMapLock->lockShared(StopTheWorldSnapshotterWithoutDCL::NUM_LOCK_FOR_STOP_THE_WORLD - 1);

    persist_func::clwb(addr);
    markChunk(addr, CACHE_LINE_SIZE);
    
    memMapLock->unlockShared(StopTheWorldSnapshotterWithoutDCL::NUM_LOCK_FOR_STOP_THE_WORLD - 1);
}

void StopTheWorldSnapshotterWithoutDCL::clwbRangeNofenceLocked(void* addr, uint64_t size) {
    memMapLock->lockShared(StopTheWorldSnapshotterWithoutDCL::NUM_LOCK_FOR_STOP_THE_WORLD - 1);
    
    persist_func::clwb_range_nofence(addr, size);
    markChunk(addr, size);
    
    memMapLock->unlockShared(StopTheWorldSnapshotterWithoutDCL::NUM_LOCK_FOR_STOP_THE_WORLD - 1);
}

/**
 * StopTheWorldSnapshotterWithDCL
 */
StopTheWorldSnapshotterWithDCL::StopTheWorldSnapshotterWithDCL(GlobalTestConfig* _gtc,
            std::string _heap_file_id, char* _heap_base_adr[]):
isSnapshotterWorking(false),
StopTheWorldSnapshotterWithoutDCL(_gtc, _heap_file_id, _heap_base_adr)
{ 
}

void StopTheWorldSnapshotterWithDCL::clwbProtected(void* addr) {
    bool copyToBackup = false;

    if (!isSnapshotterWorking.ui.load()) {

        persist_func::clwb(addr);
        markChunk(addr, CACHE_LINE_SIZE);

        if (isSnapshotterWorking.ui.load())
            copyToBackup = true;
    } else {
        clwbLocked(addr);
    }

    if (copyToBackup) clwbLocked(addr);
}

void StopTheWorldSnapshotterWithDCL::clwbRangeNofenceProtected(void* addr, uint64_t size) {
    bool copyToBackup = false;

    if (!isSnapshotterWorking.ui.load()) {
        persist_func::clwb_range_nofence(addr, size);
        markChunk(addr, size);

        if (isSnapshotterWorking.ui.load())
            copyToBackup = true;
    } else {
        clwbRangeNofenceLocked(addr, size);
    }

    if (copyToBackup) clwbRangeNofenceLocked(addr, size);
}

void StopTheWorldSnapshotterWithDCL::takeSnapshot() {
    isSnapshotterWorking.ui.store(true);
    StopTheWorldSnapshotterWithoutDCL::takeSnapshot();
    isSnapshotterWorking.ui.store(false);
}