#ifndef MEMMAPSNAPSHOTTER_HPP
#define MEMMAPSNAPSHOTTER_HPP

#include <fcntl.h>
#include <sys/mman.h>

#include <filesystem>
#include <string>
#include <thread>
#include <atomic>
#include <shared_mutex>
#include <utility>
#include <mutex>
#include <condition_variable>

#include "TestConfig.hpp"
#include "pm_config.hpp"
#include "ralloc.hpp"
#include "ConcurrentPrimitives.hpp"
#include "PersistFunc.hpp"


namespace pds {
    class EpochAdvancer;
    class EpochSys;
}

/**
 * Abstract memory-mapped file snapshotter class
 */
class MemMapSnapshotter {
public:
    static constexpr int64_t FIRST_SNAPSHOT_IDX = 0;
    static constexpr int64_t FIRST_CHUNK_IDX = 0;
    static constexpr int64_t NOT_INITIALIZED = -1;

    class MemMapLock {
    private:
        typedef std::shared_mutex Lock;        
        uint64_t size;
        
        Lock* locks;
    public:

        inline MemMapLock(uint64_t _size);

        inline void lockShared(uint64_t chunkId);
        inline void unlockShared(uint64_t chunkId);
        
        inline void lockExclusive(uint64_t chunkId);
        inline void unlockExclusive(uint64_t chunkId);

        inline ~MemMapLock();
    };

    class MemMapChunkInfo {
    public:

        struct MemMapInfo {
            paddedAtomic<void*> adr;
            paddedAtomic<int> fd;

            MemMapInfo(void* adr_, int fd_): adr(adr_), fd(fd_) {};
            MemMapInfo(): adr(nullptr), fd(MemMapSnapshotter::NOT_INITIALIZED) {};
        };
        
        inline void closeBackup();
        inline bool isOpen();

        void* src;
        uint64_t size;
        MemMapInfo inProgress;
    };

    inline MemMapSnapshotter(GlobalTestConfig* _gtc, std::string _heap_file_id,
                                char* _heap_base_adr[]);
    
    static MemMapSnapshotter* getInstance(GlobalTestConfig* _gtc,
                                            std::string _heap_file_id,
                                            char* _heap_base_adr[]);

    inline void setEpochObjs(pds::EpochAdvancer* epoch_advancer_,
            pds::EpochSys* epoch_sys_);
    inline std::string getChunkFileName(int regionId, uint64_t chunkId);
    inline void initBackupMemMaps();
    inline MemMapChunkInfo::MemMapInfo getMemMap(std::string adr, uint64_t size);
    inline char* getBackupAddress(uint64_t ptr, uint64_t chunkId);
    inline void closeMemMaps();
    inline void initMemMap(uint64_t chunkId);
    inline void markChunkAndChangeBackup(void* ptr, uint64_t size);
    inline void markChunk(void* ptr, uint64_t size);

    inline void copyAll(uint64_t heap_file_idx);
    inline uint64_t getChunkIdx(uint64_t ptr);
    inline void copyChunked(uint64_t chunkId);
    
    virtual void clwbProtected(void* addr) = 0;
    virtual void clwbRangeNofenceProtected(void* addr, uint64_t size) = 0;
    virtual void takeSnapshot() = 0;

    virtual void clwbLocked(void* addr) = 0;
    virtual void clwbRangeNofenceLocked(void* addr, uint64_t size) = 0;

    void snapshotThread(int task_num);

    inline void setupMemMapConfig(char* _heap_base_adr[], 
            std::string heap_file_id, uint64_t index);

    inline void cleanUp();
protected:

    MemMapChunkInfo* chunkAddresses;

    uint64_t global_sn;
    GlobalTestConfig* gtc;
    pds::EpochAdvancer* epoch_advancer;
    pds::EpochSys* epoch_sys;
    std::thread snapshotter_thread;
    std::atomic<bool> started;
    std::string heap_files_name[LAST_IDX];
    char* heap_base_adr[LAST_IDX];
    uint64_t heap_file_size[LAST_IDX];
    uint64_t numThreads;

    uint64_t numChunk;
    uint64_t chunkSize;
    int64_t snapLength;
    std::atomic<int> collisions;
    int chunkChangedCount;

    MemMapLock* memMapLock;

    paddedAtomic<bool>* chunkChanged;
    paddedAtomic<bool>* chunkCopied;
};

/**
 * Different types of snapshotters
 */
class OnlineSnapshotterWithoutDCL : public MemMapSnapshotter {
public:
    OnlineSnapshotterWithoutDCL(GlobalTestConfig* _gtc,
                                std::string _heap_file_id,
                                char* _heap_base_adr[]);

    void clwbProtected(void* addr);
    void clwbRangeNofenceProtected(void* addr, uint64_t size);
    void clwbLocked(void* addr);
    void clwbRangeNofenceLocked(void* addr, uint64_t size);

    void takeSnapshot();
};

class OnlineSnapshotterWithDCL : public OnlineSnapshotterWithoutDCL {
private:
    paddedAtomic<bool> isSnapshotterWorking;

public:
    OnlineSnapshotterWithDCL(GlobalTestConfig* _gtc,
                                std::string _heap_file_id,
                                char* _heap_base_adr[]);

    void clwbProtected(void* addr);
    void clwbRangeNofenceProtected(void* addr, uint64_t size);

    void takeSnapshot();
};

class StopTheWorldSnapshotterWithoutDCL : public MemMapSnapshotter {
public:
    static constexpr int64_t NUM_LOCK_FOR_STOP_THE_WORLD = 1;

    StopTheWorldSnapshotterWithoutDCL(GlobalTestConfig* _gtc,
                                        std::string _heap_file_id,
                                        char* _heap_base_adr[]);

    void clwbProtected(void* addr);
    void clwbRangeNofenceProtected(void* addr, uint64_t size);
    void clwbLocked(void* addr);
    void clwbRangeNofenceLocked(void* addr, uint64_t size);

    void takeSnapshot();
};

class StopTheWorldSnapshotterWithDCL : public StopTheWorldSnapshotterWithoutDCL {
private:
    paddedAtomic<bool> isSnapshotterWorking;

public:
    StopTheWorldSnapshotterWithDCL(GlobalTestConfig* _gtc,
                                    std::string _heap_file_id,
                                    char* _heap_base_adr[]);

    void clwbProtected(void* addr);
    void clwbRangeNofenceProtected(void* addr, uint64_t size);

    void takeSnapshot();
};

/**
 * Helper inline functions
 */
bool MemMapSnapshotter::MemMapChunkInfo::isOpen() {
    return inProgress.fd.ui.load() != MemMapSnapshotter::NOT_INITIALIZED;
}

void MemMapSnapshotter::MemMapChunkInfo::closeBackup() {
    munmap(inProgress.adr.ui.load(), size);
    close(inProgress.fd.ui.load());

    inProgress.adr.ui.store(nullptr);
    inProgress.fd.ui.store(MemMapSnapshotter::NOT_INITIALIZED);
}

MemMapSnapshotter::MemMapLock::MemMapLock(uint64_t _size): size(_size) {
    locks = new Lock[size];
}

void MemMapSnapshotter::MemMapLock::lockShared(uint64_t chunkId) {
    locks[chunkId].lock_shared();
}

void MemMapSnapshotter::MemMapLock::unlockShared(uint64_t chunkId) {
    locks[chunkId].unlock_shared();
}

void MemMapSnapshotter::MemMapLock::lockExclusive(uint64_t chunkId) {
    locks[chunkId].lock();
}

void MemMapSnapshotter::MemMapLock::unlockExclusive(uint64_t chunkId) {
    locks[chunkId].unlock();
}

MemMapSnapshotter::MemMapLock::~MemMapLock() {
    delete[] locks;
}

void MemMapSnapshotter::setupMemMapConfig(char* _heap_base_adr[],
        std::string heap_file_id, uint64_t index) {

    heap_base_adr[index] = _heap_base_adr[index];

    switch (index) {
        case DESC_IDX:
            heap_files_name[DESC_IDX] = heap_file_id + "_desc";
            break;
        case SB_IDX:
            heap_files_name[SB_IDX] = heap_file_id + "_sb";
            break;
        case META_IDX:
            heap_files_name[META_IDX] = heap_file_id + "_basemd";
            break;
        default:
            break;
    }
    
    std::filesystem::path filepath(HEAPFILE_PREFIX + heap_files_name[index]);
    heap_file_size[index] = std::filesystem::file_size(filepath);
}

MemMapSnapshotter::MemMapSnapshotter(GlobalTestConfig* _gtc,
        std::string heap_file_id, char* _heap_base_adr[])
: global_sn(MemMapSnapshotter::FIRST_SNAPSHOT_IDX),
gtc(_gtc){

    // chunkSize's unit is MB
    chunkSize = atoi(gtc->getEnv("chunkSize").c_str()) << 20;

    // chunkSize should be divisible by SBSIZE
    // in order to assign each superblock to one chunk.
    chunkSize = (chunkSize / SBSIZE) * SBSIZE;

    numThreads = atoi(gtc->getEnv("spThreads").c_str());

    for (uint64_t i = 0; i < LAST_IDX; i++) {
        setupMemMapConfig(_heap_base_adr, heap_file_id, i);
    }
    
    numChunk = heap_file_size[SB_IDX] / chunkSize + 1;
    chunkChanged = new paddedAtomic<bool>[numChunk];
    chunkCopied = new paddedAtomic<bool>[numChunk];

    for (uint64_t chunkId = 0; chunkId < numChunk; chunkId++) {
        chunkChanged[chunkId].ui.store(true);
        chunkCopied[chunkId].ui.store(false);
    }

    printf("%s,", gtc->getEnv("chunkSize").c_str());
}

void MemMapSnapshotter::setEpochObjs(
        pds::EpochAdvancer* epoch_advancer_,
        pds::EpochSys* epoch_sys_) {

    epoch_advancer = epoch_advancer_;
    epoch_sys = epoch_sys_;
}

void MemMapSnapshotter::closeMemMaps() {
    for (uint64_t chunkId = 0; chunkId < numChunk; chunkId++) {
        if (chunkAddresses[chunkId].isOpen()) {
            chunkAddresses[chunkId].closeBackup();
        }
        chunkCopied[chunkId].ui.store(false);
    }
}

MemMapSnapshotter::MemMapChunkInfo::MemMapInfo
        MemMapSnapshotter::getMemMap(std::string adr, uint64_t size) {

    int destFd = open(adr.c_str(), O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
    
    if (destFd == -1) {
        std::cerr << "Failed to open destination file" << std::endl;
        return MemMapChunkInfo::MemMapInfo();
    }

    // Extend the size of the destination file to match the size of the source file
    if (ftruncate(destFd, size) == -1) {
        std::cerr << "Failed to extend destination file size" << std::endl;
        close(destFd);
        return MemMapChunkInfo::MemMapInfo();
    }

    // Memory-map the destination file
    void* destMemMap = mmap(nullptr, size, PROT_WRITE, MAP_SHARED, destFd, 0);
    if (destMemMap == MAP_FAILED) {
        std::cerr << "Failed to memory-map destination file" << std::endl;
        close(destFd);
        return MemMapChunkInfo::MemMapInfo();
    }

    return MemMapChunkInfo::MemMapInfo(destMemMap, destFd);
}

std::string MemMapSnapshotter::getChunkFileName(int regionId, uint64_t chunkId) {
    return gtc->getEnv("backupFile") +
            heap_files_name[regionId] + "_" +
            std::to_string(global_sn) + "_" + 
            std::to_string(chunkId);
}

void MemMapSnapshotter::initBackupMemMaps() {

    chunkAddresses = new MemMapChunkInfo[numChunk];

    for (uint64_t chunkId = 0; chunkId < numChunk; chunkId++) {

        uint64_t begin = chunkId * chunkSize;
        uint64_t end = std::min((chunkId + 1) * chunkSize,
                heap_file_size[SB_IDX]);

        uint64_t srcSize = end - begin;

        // The following two will not be changed during the execution
        chunkAddresses[chunkId].src = heap_base_adr[SB_IDX] + begin;
        chunkAddresses[chunkId].size = srcSize;
    }
}

void MemMapSnapshotter::copyAll(uint64_t heap_file_idx) {

    char* srcMemMap = heap_base_adr[heap_file_idx];
    uint64_t srcSize = heap_file_size[heap_file_idx];

    std::string dest = getChunkFileName(heap_file_idx, MemMapSnapshotter::FIRST_CHUNK_IDX);
    MemMapChunkInfo::MemMapInfo destMemMap = getMemMap(dest, srcSize);

    std::memcpy((char*)destMemMap.adr.ui.load(), (char*)srcMemMap, srcSize);
    munmap(destMemMap.adr, srcSize);
    close(destMemMap.fd);
}

void MemMapSnapshotter::markChunkAndChangeBackup(void* ptr, uint64_t size) {
    
    uint64_t chunkId = getChunkIdx((uint64_t)ptr);

    if (chunkCopied[chunkId].ui.load()) {
        if (chunkAddresses[chunkId].inProgress.fd.ui.load() == MemMapSnapshotter::NOT_INITIALIZED) {
            initMemMap(chunkId);
        }

        char* backupAdr = getBackupAddress((uint64_t)ptr, chunkId);
        uint64_t end = (((uint64_t)ptr + size) | CACHE_LINE_MASK);
        collisions++;
        std::memcpy(backupAdr, (char*)ptr, end - (uint64_t)ptr + 1);

    } else {
        chunkChanged[chunkId].ui.store(true);
    }
}

void MemMapSnapshotter::markChunk(void* ptr, uint64_t size) {
    
    uint64_t chunkId = getChunkIdx((uint64_t)ptr);

    if (!chunkChanged[chunkId].ui.load()) {
        chunkChanged[chunkId].ui.store(true);
    }
}

void MemMapSnapshotter::copyChunked(uint64_t chunkId) {

    std::memcpy((char*)chunkAddresses[chunkId].inProgress.adr.ui.load(),
            (char*)chunkAddresses[chunkId].src,
            chunkAddresses[chunkId].size);

    chunkChanged[chunkId].ui.store(false);
}

void MemMapSnapshotter::initMemMap(uint64_t chunkId) {

    std::string destName = getChunkFileName(SB_IDX, chunkId);
    assert(chunkAddresses[chunkId].inProgress.adr == nullptr);
    
    MemMapChunkInfo::MemMapInfo destMemMap = getMemMap(destName, chunkAddresses[chunkId].size);
    
    chunkAddresses[chunkId].inProgress.adr.ui.store(destMemMap.adr.ui.load());
    chunkAddresses[chunkId].inProgress.fd.ui.store(destMemMap.fd.ui.load());
}

char* MemMapSnapshotter::getBackupAddress(uint64_t ptr, uint64_t chunkId) {
    uint64_t offset = ptr - (uint64_t)chunkAddresses[chunkId].src;

    return (char*)chunkAddresses[chunkId].inProgress.adr.ui.load() + offset;
}

uint64_t MemMapSnapshotter::getChunkIdx(uint64_t ptr) {
    uint64_t offset = ptr - (uint64_t)heap_base_adr[SB_IDX];
    return offset / chunkSize;
}

void MemMapSnapshotter::cleanUp() {
    started.store(false);
    // flush and quit dedicated mem map snapshotter
    if (snapshotter_thread.joinable()){
        snapshotter_thread.join();
    }

    delete[] chunkChanged;
    delete[] chunkCopied;
    delete[] chunkAddresses;
}

#endif // MEMMAPSNAPSHOTTER_HPP